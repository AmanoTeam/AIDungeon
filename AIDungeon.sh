#!/usr/bin/env bash

declare -rg DatabasePath="${HOME}/AIDungeon/databases/gamedata.db"
declare -rg CACertificateFile="${HOME}/AIDungeon/certificates/cacert.pem"
declare -rg CACertificateDirectory="${HOME}/AIDungeon/certificates"
declare -rg DialogsFilePath="${HOME}/AIDungeon/dialogs/en.json"
declare -rg TranslateShellPath="${HOME}/AIDungeon/dependencies/translate-shell.sh"
declare -rg LanguagesFilePath="${HOME}/AIDungeon/dependencies/languages.json"

source "${HOME}/AIDungeon/functions/aidungeon.sh"
source "${HOME}/AIDungeon/functions/shell_bot.sh"

if ! [ -s "${DatabasePath}" ]; then
	mkdir -p "${HOME}/AIDungeon/databases"
	sqlite3 "${DatabasePath}" 'CREATE TABLE data (
		user_id INTEGER,
		user_status TEXT,
		user_preferred_lang TEXT,
		game_status TEXT,
		current_input TEXT,
		story_type TEXT,
		character_type TEXT,
		character_name TEXT,
		memory_context TEXT,
		aidungeon_api_session_id INTEGER );'
fi

chmod --recursive '700' "${HOME}/AIDungeon"

init --token 'YOU_TOKEN_HERE'
 
while true; do

	getUpdates --limit '1' --offset "$(OffsetNext)" --timeout '60'

	(

		if [ -z "${message_text}" ]; then
			exit '0'
		elif [ -n "${message_reply_to_message_message_id}" ]; then
			exit '0'
		fi
		
		if [[ "${message_text}" =~ ^(\!|/)'start'$ ]]; then
			check_if_can_use_commands
			sendMessage --reply_to_message_id "${message_message_id}" \
										--chat_id "${message_chat_id}" \
										--text "$(get_dialog '.dialogs.beginning')" \
										--parse_mode 'markdown' \
										--disable_web_page_preview 'true' \
										--disable_notification 'true'
		elif [[ "${message_text}" =~ ^(\!|/)'new_game'$ ]]; then
			check_if_can_use_commands
			sqlite3 "${DatabasePath}" "UPDATE data SET game_status = 'playing' WHERE user_id = '${message_chat_id}';"
			action_pick_a_setting
		elif [[ "${message_text}" =~ ^(\!|/)'end_game'$ ]]; then
			check_if_new_user
			if [[ $(sqlite3 "${DatabasePath}" "SELECT game_status FROM data WHERE user_id = '${message_chat_id}';") = 'playing' ]]; then
				purge_session_data
			else
				sendMessage --reply_to_message_id "${message_message_id}" \
											--chat_id "${message_chat_id}" \
											--text "$(get_dialog '.dialogs.not_playing_error')" \
											--disable_notification 'true'
				exit '1'
			fi
		elif [[ "${message_text}" =~ ^(\!|/)'help'$ ]]; then
			check_if_can_use_commands
			sendMessage --reply_to_message_id "${message_message_id}" \
										--chat_id "${message_chat_id}" \
										--text "$(get_dialog '.dialogs.help')" \
										--parse_mode 'markdown' \
										--disable_notification 'true'
			exit '0' 
		elif [[ "${message_text}" =~ ^(\!|/)'continue'$ ]]; then
			check_if_new_user
			continue_game
		elif [[ "${message_text}" =~ ^(\!|/)'revert'$ ]]; then
			if [[ $(sqlite3 "${DatabasePath}" "SELECT aidungeon_api_session_id FROM data WHERE user_id = '${message_chat_id}';") =~ [0-9]+ ]]; then
				declare -r json_data="{"\""text"\"":"\""/revert"\""}"
				send_message_to_aidungeon_api
			else
				sendMessage --reply_to_message_id "${message_message_id}" \
											--chat_id "${message_chat_id}" \
											--text "$(get_dialog '.dialogs.not_playing_error')" \
											--disable_notification 'true'
				exit '1'
			fi
		elif [[ "${message_text}" =~ ^(\!|/)'support'$ ]]; then
			check_if_can_use_commands
			sendMessage --reply_to_message_id "${message_message_id}" \
										--chat_id "${message_chat_id}" \
										--text "$(get_dialog '.dialogs.support_developer')" \
										--disable_web_page_preview 'true' \
										--disable_notification 'true'
			exit '0'
		elif [[ "${message_text}" =~ ^(\!|/)'remember'$ ]]; then
			check_if_new_user
			if [[ $(sqlite3 "${DatabasePath}" "SELECT aidungeon_api_session_id FROM data WHERE user_id = '${message_chat_id}';") =~ [0-9]+ ]]; then
				declare -r memory_context=$(sqlite3 "${DatabasePath}" "SELECT memory_context FROM data WHERE user_id = '${message_chat_id}';")
				sendMessage --reply_to_message_id "${message_message_id}" \
											--chat_id "${message_chat_id}" \
											--text "${memory_context}" \
											--disable_notification 'true'
				exit '0'
			else
				sendMessage --reply_to_message_id "${message_message_id}" \
											--chat_id "${message_chat_id}" \
											--text "$(get_dialog '.dialogs.not_playing_error')" \
											--disable_notification 'true'
				exit '1'
			fi
		elif [[ "${message_text}" =~ ^(\!|/)'remember'.+ ]]; then
			check_if_new_user
			if [[ $(sqlite3 "${DatabasePath}" "SELECT aidungeon_api_session_id FROM data WHERE user_id = '${message_chat_id}';") =~ [0-9]+ ]]; then
				declare -r message_to_remember=$(sed -r 's/^(\!|\/)remember\s(.*)/\2/g' <<< "${message_text}")
				change_memory_context
				sqlite3 "${DatabasePath}" "UPDATE data SET memory_context = '${message_to_remember}' WHERE user_id = '${message_chat_id}';"
				if [ "${?}" = '0' ]; then
					sendMessage --reply_to_message_id "${message_message_id}" \
											--chat_id "${message_chat_id}" \
											--text "$(get_dialog '.dialogs.memory_context_saved')" \
											--disable_notification 'true'
					exit '0'
				else
					sendMessage --reply_to_message_id "${message_message_id}" \
												--chat_id "${message_chat_id}" \
												--text "$(get_dialog '.dialogs.memory_context_not_saved_error')" \
												--disable_notification 'true'
					purge_session_data
					exit '1'
				fi
				
			else
				sendMessage --reply_to_message_id "${message_message_id}" \
											--chat_id "${message_chat_id}" \
											--text "$(get_dialog '.dialogs.not_playing_error')" \
											--disable_notification 'true'
				exit '1'
			fi
		elif [[ "${message_text}" =~ ^(\!|/)'change_language'$ ]]; then
			check_if_new_user
			declare -r language_id=$(sqlite3 "${DatabasePath}" "SELECT user_preferred_lang FROM data WHERE user_id = '${message_chat_id}';")
			sendMessage --reply_to_message_id "${message_message_id}" \
										--chat_id "${message_chat_id}" \
										--text "$(get_dialog '.dialogs.your_current_lang') \`${language_id} ($(get_lang_name))\`" \
										--parse_mode 'markdown' \
										--disable_notification 'true'
			exit '0'
		elif [[ "${message_text}" =~ ^(\!|/)'change_language'.+ ]]; then
			check_if_new_user
			change_language
		else
			check_if_new_user
			if [[ $(sqlite3 "${DatabasePath}" "SELECT game_status FROM data WHERE user_id = '${message_chat_id}';") = 'playing' ]]; then
				parse_user_input
			else
				sendMessage --reply_to_message_id "${message_message_id}" \
											--chat_id "${message_chat_id}" \
											--text "$(get_dialog '.dialogs.not_playing_error')" \
											--disable_notification 'true'
				exit '1'
			fi
		fi

		exit '0'

	) &

done
