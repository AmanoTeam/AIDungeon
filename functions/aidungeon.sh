#!/usr/bin/env bash

function check_if_can_use_commands()
{

	if [[ $(sqlite3 "${DatabasePath}" "SELECT user_status FROM data WHERE user_id = '${message_chat_id}';") != 'valid' ]]; then
		sqlite3 "${DatabasePath}" "INSERT INTO data VALUES(
			'${message_chat_id}',
			'valid',
			'en',
			'none',
			'none',
			'none',
			'none',
			'none',
			'none',
			'none' );"
	elif [[ $(sqlite3 "${DatabasePath}" "SELECT game_status FROM data WHERE user_id = '${message_chat_id}';") = 'playing' ]]; then
		sendMessage --chat_id "${message_chat_id}" \
								--reply_to_message_id "${message_message_id}" \
								--text "$(get_dialog '.dialogs.cannot_use_commands_game_running')" \
								--disable_notification 'true'
		exit '1'
	fi

}

declare -rgf 'check_if_can_use_commands'

function check_if_new_user()
{

	if [[ $(sqlite3 "${DatabasePath}" "SELECT user_status FROM data WHERE user_id = '${message_chat_id}';") != 'valid' ]]; then
		sqlite3 "${DatabasePath}" "INSERT INTO data VALUES(
			'${message_chat_id}',
			'valid',
			'en',
			'none',
			'none',
			'none',
			'none',
			'none',
			'none',
			'none' );"
	fi

}

declare -rgf 'check_if_new_user'

function action_pick_a_setting()
{

	sqlite3 "${DatabasePath}" "UPDATE data SET current_input = 'pick_a_setting' WHERE user_id = '${message_chat_id}';"

	sendMessage --reply_to_message_id "${message_message_id}" \
								--chat_id "${message_chat_id}" \
								--text "$(get_dialog '.dialogs.pick_a_setting')" \
								--disable_notification 'true'

	exit '0'

}

declare -rgf 'action_pick_a_setting'

function action_fantasy_pick_character()
{

	sqlite3 "${DatabasePath}" "UPDATE data SET current_input = 'fantasy_pick_character' WHERE user_id = '${message_chat_id}';"
	
	sendMessage --reply_to_message_id "${message_message_id}" \
								--chat_id "${message_chat_id}" \
								--text "$(get_dialog '.dialogs.fantasy_pick_character')" \
								--disable_notification 'true'

	exit '0'

}

declare -rgf 'action_fantasy_pick_character'

function action_mystery_pick_character()
{

	sqlite3 "${DatabasePath}" "UPDATE data SET current_input = 'mystery_pick_character' WHERE user_id = '${message_chat_id}';"
	
	sendMessage --reply_to_message_id "${message_message_id}" \
								--chat_id "${message_chat_id}" \
								--text "$(get_dialog '.dialogs.mystery_pick_character')" \
								--disable_notification 'true'

	exit '0'

}

declare -rgf 'action_mystery_pick_character'

function action_apocalyptic_pick_character()
{

	sqlite3 "${DatabasePath}" "UPDATE data SET current_input = 'apocalyptic_pick_character' WHERE user_id = '${message_chat_id}';"
	
	sendMessage --reply_to_message_id "${message_message_id}" \
								--chat_id "${message_chat_id}" \
								--text "$(get_dialog '.dialogs.apocalyptic_pick_character')" \
								--disable_notification 'true'

	exit '0'

}

declare -rgf 'action_apocalyptic_pick_character'

function action_zombies_pick_character()
{

	sqlite3 "${DatabasePath}" "UPDATE data SET current_input = 'zombies_pick_character' WHERE user_id = '${message_chat_id}';"
	
	sendMessage --reply_to_message_id "${message_message_id}" \
								--chat_id "${message_chat_id}" \
								--text "$(get_dialog '.dialogs.zombies_pick_character')" \
								--disable_notification 'true'

	exit '0'

}

declare -rgf 'action_zombies_pick_character'

function action_chose_character_name()
{

	sqlite3 "${DatabasePath}" "UPDATE data SET current_input = 'chose_character_name' WHERE user_id = '${message_chat_id}';"
	
	sendMessage --reply_to_message_id "${message_message_id}" \
								--chat_id "${message_chat_id}" \
								--text "$(get_dialog '.dialogs.enter_character_name')" \
								--disable_notification 'true'

	exit '0'

}

declare -rgf 'action_chose_character_name'

function begin_game_session()
{

	if [ "${current_input}" != 'set_custom_story' ]; then
		declare -r story_type=$(sqlite3 "${DatabasePath}" "SELECT story_type FROM data WHERE user_id = '${message_chat_id}';")
		declare -r character_type=$(sqlite3 "${DatabasePath}" "SELECT character_type FROM data WHERE user_id = '${message_chat_id}';")
		declare -r character_name=$(sqlite3 "${DatabasePath}" "SELECT character_name FROM data WHERE user_id = '${message_chat_id}';")
		declare -r json_data="{"\""storyMode"\"":"\""${story_type}"\"","\""characterType"\"":"\""${character_type}"\"","\""name"\"":"\""${character_name}"\"","\""customPrompt"\"":null,"\""promptId"\"":null}"
	else
		if ! [[ "${user_lang_code}" =~ (none|en) ]]; then
			declare -r user_lang_code=$(sqlite3 "${DatabasePath}" "SELECT user_preferred_lang FROM data WHERE user_id = '${message_chat_id}';")
			declare -r translated_input=$(translate_input <<< "${message_text}")
			declare -r json_data="{"\""storyMode"\"":"\""custom"\"","\""characterType"\"":null,"\""name"\"":null,"\""customPrompt"\"":"\""${translated_input}"\"","\""promptId"\"":null}"
			sqlite3 "${DatabasePath}" "UPDATE data SET current_input = 'interacting_with_aidungeon_api' WHERE user_id = '${message_chat_id}';"
		else
			declare -r json_data="{"\""storyMode"\"":"\""custom"\"","\""characterType"\"":null,"\""name"\"":null,"\""customPrompt"\"":"\""${message_text}"\"","\""promptId"\"":null}"
			sqlite3 "${DatabasePath}" "UPDATE data SET current_input = 'interacting_with_aidungeon_api' WHERE user_id = '${message_chat_id}';"
		fi
	fi
	
	declare -r api_response=$(mktemp --dry-run)
	
	curl --url 'https://api.aidungeon.io/sessions' \
			--resolve 'api.aidungeon.io:443:104.27.169.66' \
			--request 'POST' \
			--silent \
			--http2-prior-knowledge \
			--fail-early \
			--tlsv1.3 \
			--header 'accept' \
			--header 'content-type: application/json' \
			--header 'x-access-token: 2ab6afb0-61b2-11ea-ae2f-81a68247e59e' \
			--user-agent 'okhttp/3.12.1' \
			--ipv4 \
			--connect-timeout '25' \
			--cacert "${CACertificateFile}" \
			--capath "${CACertificateDirectory}" \
			--no-sessionid \
			--no-keepalive \
			--data "${json_data}" \
			--output "${api_response}"
	
	if [[ -z $(jq --raw-output '.' "${api_response}") ]]; then
		sendMessage --reply_to_message_id "${message_message_id}" \
									--chat_id "${message_chat_id}" \
									--text "$(get_dialog '.dialogs.invalid_json_response_error')" \
									--disable_notification 'true'
		sqlite3 "${DatabasePath}" "DELETE FROM data WHERE user_id = '${message_chat_id}';"
		exit '1'
	else
		declare -r session_id=$(jq --raw-output '.id' "${api_response}")
		declare -r memory_context=$(jq --raw-output '.context[]' "${api_response}")
		if [ -n "${memory_context}" ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET memory_context = '${memory_context}' WHERE user_id = '${message_chat_id}';"
		fi
	fi
	
	if [[ "${session_id}" =~ [0-9]+ ]]; then
		sqlite3 "${DatabasePath}" "UPDATE data SET aidungeon_api_session_id = '${session_id}' WHERE user_id = '${message_chat_id}';"
		sendMessage --reply_to_message_id "${message_message_id}" \
									--chat_id "${message_chat_id}" \
									--text "$(jq --raw-output '.story[].value' "${api_response}" | translate_output)" \
									--disable_notification 'true'
		exit '0'
	else
		sendMessage --reply_to_message_id "${message_message_id}" \
									--chat_id "${message_chat_id}" \
									--text "$(get_dialog '.dialogs.sessionid_not_created_error')" \
									--disable_notification 'true'
		sqlite3 "${DatabasePath}" "DELETE FROM data WHERE user_id = '${message_chat_id}';"
		exit '1'
	fi

}

declare -rgf 'begin_game_session'

function send_message_to_aidungeon_api()
{

	declare -r session_id=$(sqlite3 "${DatabasePath}" "SELECT aidungeon_api_session_id FROM data WHERE user_id = '${message_chat_id}';")
	
	declare -r api_response=$(mktemp --dry-run)
		
	curl --url "https://api.aidungeon.io/sessions/${session_id}/inputs" \
			--resolve 'api.aidungeon.io:443:104.27.169.66' \
			--request 'POST' \
			--silent \
			--http2-prior-knowledge \
			--tlsv1.3 \
			--header 'accept' \
			--header 'content-type: application/json' \
			--header 'x-access-token: 2ab6afb0-61b2-11ea-ae2f-81a68247e59e' \
			--user-agent 'okhttp/3.12.1' \
			--ipv4 \
			--connect-timeout '25' \
			--cacert "${CACertificateFile}" \
			--capath "${CACertificateDirectory}" \
			--no-sessionid \
			--no-keepalive \
			--data "${json_data}" \
			--output "${api_response}"
	
	if [[ -z $(jq --raw-output '.' "${api_response}") ]]; then
		sendMessage --reply_to_message_id "${message_message_id}" \
									--chat_id "${message_chat_id}" \
									--text "$(get_dialog '.dialogs.invalid_json_response_error')" \
									--disable_notification 'true'
		sqlite3 "${DatabasePath}" "DELETE FROM data WHERE user_id = '${message_chat_id}';"
		exit '1'
	elif [[ $(jq --raw-output '.' "${api_response}") = 'You'\''ve reverted this story as far as possible.' ]]; then
		sendMessage --reply_to_message_id "${message_message_id}" \
									--chat_id "${message_chat_id}" \
									--text "$(jq --raw-output '.' "${api_response}" | translate_output)" \
									--disable_notification 'true'
		exit '1'
	else
		sendMessage --reply_to_message_id "${message_message_id}" \
									--chat_id "${message_chat_id}" \
									--text "$(jq --raw-output '.[-1].value' "${api_response}" | translate_output)" \
									--disable_notification 'true'
		exit '0'
	fi


}

declare -rgf 'send_message_to_aidungeon_api'

function get_dialog()
{

	jq --raw-output "$1" "${DialogsFilePath}"

}

declare -rfg 'get_dialog'

function translate_output()
{
	
	declare -r user_lang_code=$(sqlite3 "${DatabasePath}" "SELECT user_preferred_lang FROM data WHERE user_id = '${message_chat_id}';")
	if ! [[ "${user_lang_code}" =~ (none|en) ]]; then
		if [[ "${user_lang_code}" =~ [A-Za-z]{2,4} ]]; then
			"${TranslateShellPath}" -no-init \
						-no-pager \
						-no-autocorrect \
						-inet4-only \
						-user-agent 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36' \
						-show-original 'n' \
						-show-original-phonetics 'n' \
						-show-translation-phonetics 'n' \
						-show-prompt-message 'n' \
						-show-languages 'n' \
						-show-original-dictionary 'n' \
						-show-dictionary 'n' \
						-show-alternatives 'n' \
						-from 'en' -to "${user_lang_code}" | sed -e 's/\x1b\[.\{1,5\}m//g'
		else
			tee
		fi
	else
		tee
	fi

}

declare -rfg 'translate_output'

function purge_session_data()
{

	declare -r session_id=$(sqlite3 "${DatabasePath}" "SELECT aidungeon_api_session_id FROM data WHERE user_id = '${message_chat_id}';")
	
	curl --url "https://api.aidungeon.io/sessions/${session_id}" \
			--resolve 'api.aidungeon.io:443:104.27.169.66' \
			--request 'DELETE' \
			--silent \
			--http2-prior-knowledge \
			--tlsv1.3 \
			--header 'accept' \
			--header 'content-type: application/json' \
			--header 'x-access-token: 2ab6afb0-61b2-11ea-ae2f-81a68247e59e' \
			--user-agent 'okhttp/3.12.1' \
			--ipv4 \
			--connect-timeout '25' \
			--cacert "${CACertificateFile}" \
			--capath "${CACertificateDirectory}" \
			--no-sessionid \
			--no-keepalive \
			--data "${json_data}" \
			--output '/dev/null'

	sqlite3 "${DatabasePath}" "DELETE FROM data WHERE user_id = '${message_chat_id}';"

	if [ "${?}" = '0' ]; then
		sendMessage --reply_to_message_id "${message_message_id}" \
									--chat_id "${message_chat_id}" \
									--text "$(get_dialog '.dialogs.purge_data_success')" \
									--disable_notification 'true'
		exit '0'
	else
		sendMessage --reply_to_message_id "${message_message_id}" \
									--chat_id "${message_chat_id}" \
									--text "$(get_dialog '.dialogs.cannot_purge_data_error')" \
									--disable_notification 'true'
		exit '1'
	fi

}

declare -rgf 'purge_session_data'

function action_set_custom_story()
{

	sendMessage --reply_to_message_id "${message_message_id}" \
								--chat_id "${message_chat_id}" \
								--text "$(get_dialog '.dialogs.set_custom_story')" \
								--disable_notification 'true'

	exit '0'

}

function parse_user_input()
{

	if [[ $(sqlite3 "${DatabasePath}" "SELECT aidungeon_api_session_id FROM data WHERE user_id = '${message_chat_id}';") =~ [0-9]+ ]]; then
		declare -r user_lang_code=$(sqlite3 "${DatabasePath}" "SELECT user_preferred_lang FROM data WHERE user_id = '${message_chat_id}';")
		if ! [[ "${user_lang_code}" =~ (none|en) ]]; then
			declare -r translated_input=$(translate_input <<< "${message_text}")
			declare -r json_data="{"\""text"\"":"\""${translated_input}"\""}"
			send_message_to_aidungeon_api
		else
			declare -r json_data="{"\""text"\"":"\""${message_text}"\""}"
			send_message_to_aidungeon_api
		fi
	else
		declare -r current_input=$(sqlite3 "${DatabasePath}" "SELECT current_input FROM data WHERE user_id = '${message_chat_id}';")
	fi

	if [ "${current_input}" = 'pick_a_setting' ]; then
		if [ "${message_text}" = '1' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET story_type = 'fantasy' WHERE user_id = '${message_chat_id}';"
			action_fantasy_pick_character
		elif [ "${message_text}" = '2' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET story_type = 'mystery' WHERE user_id = '${message_chat_id}';"
			action_mystery_pick_character
		elif [ "${message_text}" = '3' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET story_type = 'apocalyptic' WHERE user_id = '${message_chat_id}';"
			action_apocalyptic_pick_character
		elif [ "${message_text}" = '4' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET story_type = 'zombies' WHERE user_id = '${message_chat_id}';"
			action_zombies_pick_character
		elif [ "${message_text}" = '5' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET current_input = 'set_custom_story' WHERE user_id = '${message_chat_id}';"
			action_set_custom_story
		else
			sendMessage --chat_id "${message_chat_id}" \
										--reply_to_message_id "${message_message_id}" \
										--text "$(get_dialog '.dialogs.invalid_option_error')" \
										--disable_notification 'true'
			exit '1'
		fi
	elif [ "${current_input}" = 'fantasy_pick_character' ]; then
		if [ "${message_text}" = '1' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'noble' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '2' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'knight' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '3' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'squire' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '4' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'wizard' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '5' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'ranger' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '6' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'peasant' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '7' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'rogue' WHERE user_id = '${message_chat_id}';"
		else
			sendMessage --chat_id "${message_chat_id}" \
										--reply_to_message_id "${message_message_id}" \
										--text "$(get_dialog '.dialogs.invalid_option_error')" \
										--disable_notification 'true'
			exit '1'
		fi
	elif [ "${current_input}" = 'mystery_pick_character' ]; then
		if [ "${message_text}" = '1' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'patient' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '2' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'detective' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '3' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'spy' WHERE user_id = '${message_chat_id}';"
		else
			sendMessage --chat_id "${message_chat_id}" \
										--reply_to_message_id "${message_message_id}" \
										--text "$(get_dialog '.dialogs.invalid_option_error')" \
										--disable_notification 'true'
			exit '1'
		fi
	elif [ "${current_input}" = 'apocalyptic_pick_character' ]; then
		if [ "${message_text}" = '1' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'soldier' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '2' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'scavenger' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '3' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'survivor' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '4' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'courier' WHERE user_id = '${message_chat_id}';"
		else
			sendMessage --chat_id "${message_chat_id}" \
										--reply_to_message_id "${message_message_id}" \
										--text "$(get_dialog '.dialogs.invalid_option_error')" \
										--disable_notification 'true'
			exit '1'
		fi
	elif [ "${current_input}" = 'zombies_pick_character' ]; then
		if [ "${message_text}" = '1' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'soldier' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '2' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'survivor' WHERE user_id = '${message_chat_id}';"
		elif [ "${message_text}" = '3' ]; then
			sqlite3 "${DatabasePath}" "UPDATE data SET character_type = 'scientist' WHERE user_id = '${message_chat_id}';"
		else
			sendMessage --chat_id "${message_chat_id}" \
										--reply_to_message_id "${message_message_id}" \
										--text "$(get_dialog '.dialogs.invalid_option_error')" \
										--disable_notification 'true'
			exit '1'
		fi
	elif [ "${current_input}" = 'chose_character_name' ]; then
		sqlite3 "${DatabasePath}" "UPDATE data SET character_name = '${message_text}' WHERE user_id = '${message_chat_id}';"
		sqlite3 "${DatabasePath}" "UPDATE data SET current_input = 'interacting_with_aidungeon_api' WHERE user_id = '${message_chat_id}';"
		begin_game_session
	elif [ "${current_input}" = 'set_custom_story' ]; then
		begin_game_session
	else
		sendMessage --chat_id "${message_chat_id}" \
									--reply_to_message_id "${message_message_id}" \
									--text "$(get_dialog '.dialogs.invalid_option_error')" \
									--disable_notification 'true'
		exit '1'
	fi

	if [[ -n $(sqlite3 "${DatabasePath}" "SELECT character_type FROM data WHERE user_id = '${message_chat_id}';") ]]; then
		action_chose_character_name
	fi
	
}

declare -rgf 'parse_user_input'

function continue_game()
{

	declare -r json_data={"\""text"\"":"\"""\""}

	send_message_to_aidungeon_api
	
}

declare -rgf 'continue_game'

function change_memory_context()
{

	declare -r session_id=$(sqlite3 "${DatabasePath}" "SELECT aidungeon_api_session_id FROM data WHERE user_id = '${message_chat_id}';")
	
	declare -r json_data="{"\""context"\"":["\""${message_to_remember}"\""]}"
	
	curl --url "https://api.aidungeon.io/sessions/${session_id}" \
			--resolve 'api.aidungeon.io:443:104.27.169.66' \
			--request 'PATCH' \
			--silent \
			--http2-prior-knowledge \
			--tlsv1.3 \
			--header 'accept' \
			--header 'content-type: application/json' \
			--header 'x-access-token: 2ab6afb0-61b2-11ea-ae2f-81a68247e59e' \
			--user-agent 'okhttp/3.12.1' \
			--ipv4 \
			--connect-timeout '25' \
			--cacert "${CACertificateFile}" \
			--capath "${CACertificateDirectory}" \
			--no-sessionid \
			--no-keepalive \
			--data "${json_data}" \
			--output '/dev/null'
			
}

declare -rgf 'change_memory_context'

function get_lang_name()
{

	jq --raw-output ".languages.${language_id}.name" "${LanguagesFilePath}"

}

declare -rgf 'get_lang_name'

function change_language()
{

	declare -r language_id=$(sed -r 's/^(\!|\/)change_language\s(.*)/\2/g' <<< "${message_text}")
	
	if [[ $(jq --raw-output ".languages[\"${language_id}\"]" "${LanguagesFilePath}") = 'null' ]]; then
		sendMessage --reply_to_message_id "${message_message_id}" \
									--chat_id "${message_chat_id}" \
									--text "$(get_dialog '.dialogs.language_not_found_error')" \
									--parse_mode 'markdown' \
									--disable_web_page_preview 'true' \
									--disable_notification 'true'
		exit '1'
	fi
	
	sqlite3 "${DatabasePath}" "UPDATE data SET user_preferred_lang = '${language_id}' WHERE user_id = '${message_chat_id}';"
	
	if [ "${?}" = '0' ]; then
		sendMessage --reply_to_message_id "${message_message_id}" \
									--chat_id "${message_chat_id}" \
									--text "$(get_dialog '.dialogs.language_changed') \`${language_id} ($(get_lang_name)\`)" \
									--parse_mode 'markdown' \
									--disable_notification 'true'
		exit '0'
	else
		sendMessage --reply_to_message_id "${message_message_id}" \
									--chat_id "${message_chat_id}" \
									--text "$(get_dialog '.dialogs.language_not_changed_error')" \
									--disable_notification 'true'
		exit '1'
	fi

}

declare -rgf 'change_language'

function translate_input()
{
	
	if [[ "${user_lang_code}" =~ [A-Za-z]{2,4} ]]; then
		"${TranslateShellPath}" -no-init \
					-no-pager \
					-no-autocorrect \
					-inet4-only \
					-user-agent 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36' \
					-show-original 'n' \
					-show-original-phonetics 'n' \
					-show-translation-phonetics 'n' \
					-show-prompt-message 'n' \
					-show-languages 'n' \
					-show-original-dictionary 'n' \
					-show-dictionary 'n' \
					-show-alternatives 'n' \
					-from "${user_lang_code}" -to 'en' | sed -e 's/\x1b\[.\{1,5\}m//g' | sed -r 's/\\\s+/\\/g'
	else
		tee
	fi

}

declare -rgf 'translate_input'
